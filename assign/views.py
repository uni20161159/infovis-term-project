from django.shortcuts import render
import os, sys
import pandas as pd
# import tensorflow as tf
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
import math

datetimeindex = pd.date_range('2016-05-31 00:00:00', '2016-06-13 23:55:00', freq='5T')
bldg = pd.read_csv('assign/Data/csv/bldg-MC2.csv')

# Create your views here.
def home(request):
    data = bldg
    data_html = data.to_html()
    context = {'loaded_data': data_html}
    #return render(request, "dataflow/table.html", context)

    return render(request, 'main.html', context)


def question3(request):
    return render(request, 'Question3.html')


def q3_1(request):
    #plot_hazium()
    return render(request, 'q3_1.html')


def q3_2(request):
    bldg_co = pd.read_csv('assign/Data/csv/bldg-MC2.csv')
    dfco = bldg_co.filter(regex='CO2')
    dfco = dfco.set_index(datetimeindex)
    if os.path.exists('static/q3_2_img/norm_CO2.png'):
        return render(request, 'q3_2.html')
    else:
        plot_overlap(dfco, 'q3_2_img/CO2')
        normalize_plot(dfco, 'q3_2_img/norm_CO2')
    return render(request, 'q3_2.html')


def q3_3(request):
    dftemp = bldg.filter(regex='Temp')
    dftemp = dftemp.set_index(datetimeindex)
    #normalize(dftemp, '/q3_3_img/Temp')
    #plot_df(dftemp, 'q3_3_img/')
    return render(request, 'q3_3.html')


def q3_4(request):
    dfsup = bldg.filter(regex='SUPPLY')
    dfsup = dfsup.set_index(datetimeindex)
    normalize_plot(dfsup, '/q3_4_img/Sup_sys')
    #plot_df(dfsup, 'q3_4_img/')
    return render(request, 'q3_4.html')


def q3_5(request):

    return render(request, 'q3_5.html')


def question4(request):
    if os.path.exists('static/q4_img/prox_f3.png'):
        return render(request, 'Question4.html')
    else:
        prox_f1, prox_f2, prox_f3 = make_prox_data()
        plot_prox(prox_f1, '/q4_img/prox_f1')
        plot_prox(prox_f2, '/q4_img/prox_f2')
        plot_prox(prox_f3, '/q4_img/prox_f3')
        return render(request, 'Question4.html')


def q4_1(request):
    dfco = bldg.filter(regex='CO2')
    dfco = dfco.set_index(datetimeindex)

    prox = pd.read_csv('./assign/Data/csv/proxOut-MC2.csv')
    prox.set_index(pd.to_datetime(prox.timestamp), inplace=True)
    del prox['timestamp']

    if os.path.exists('static/q4_1_img/F_3_Z_6.png') == False:
        for f in range(1, 4):
            df = prox.loc[(prox[' floor'] == f)]
            zones = set(df[' zone'].values)
            for z in zones:
                name = "F_" + str(f) + "_Z_" + z[1:]
                df0 = df.loc[df[' zone'] == z]
                df1 = dfco.filter(regex=name)
                df0 = df0.resample('5T').sum()
                if (df1.shape[1] == 0):
                    continue;
                if (df0.shape[0] == 1):
                    continue;
                # plot
                plt.title(name + ": Prox and CO2")
                plt.xlabel('Time', fontsize=14)
                # plt.ylabel('density', fontsize=14)
                # plt.figure(figsize=[20, 5])
                df0 = normalize(df0)
                df1 = normalize(df1)
                plt.plot(df0, color='r', alpha=0.5)
                plt.plot(df1, color='g', alpha=0.5)
                # plt.legend(fontsize=14)
                # plt.tick_params(labelsize=14)
                plt.grid(True)
                plt.savefig('./static/q4_1_img/' + name)
                plt.close()

    return render(request, 'q4_1.html')


def q4_2(request):
    dfco = bldg.filter(regex='Lights')
    dfco = dfco.set_index(datetimeindex)

    prox = pd.read_csv('./assign/Data/csv/proxOut-MC2.csv')
    prox.set_index(pd.to_datetime(prox.timestamp), inplace=True)
    del prox['timestamp']

    if os.path.exists('static/q4_2_img/F_3_Z_6.png') == False:
        for f in range(1, 4):
            df = prox.loc[(prox[' floor'] == f)]
            zones = set(df[' zone'].values)
            for z in zones:
                name = "F_" + str(f) + "_Z_" + z[1:]
                df0 = df.loc[df[' zone'] == z]
                df1 = dfco.filter(regex=name)
                df0 = df0.resample('5T').sum()
                if (df1.shape[1] == 0):
                    continue;
                if (df0.shape[0] == 1):
                    continue;
                # plot
                plt.title(name + ": Prox and light power")
                plt.xlabel('Time', fontsize=14)
                # plt.ylabel('density', fontsize=14)
                # plt.figure(figsize=[20, 5])
                df0 = normalize(df0)
                df1 = normalize(df1)
                plt.plot(df0, color='r', alpha=0.7)
                plt.plot(df1, color='g', alpha=0.3)
                # plt.legend(fontsize=14)
                # plt.tick_params(labelsize=14)
                plt.grid(True)
                plt.savefig('./static/q4_2_img/' + name)
                plt.close()
    return render(request, 'q4_2.html')


def q4_3(request):
    dfreh = bldg.filter(regex='Thermostat Temp')
    dfreh = dfreh.set_index(datetimeindex)

    prox = pd.read_csv('./assign/Data/csv/proxOut-MC2.csv')
    prox.set_index(pd.to_datetime(prox.timestamp), inplace=True)
    del prox['timestamp']

    if os.path.exists('static/q4_3_img/F_3_Z_6.png') == False:
        for f in range(1, 4):
            df = prox.loc[(prox[' floor'] == f)]
            zones = set(df[' zone'].values)
            for z in zones:
                name = "F_" + str(f) + "_Z_" + z[1:]
                df0 = df.loc[df[' zone'] == z]
                df1 = dfreh.filter(regex=name)
                df0 = df0.resample('5T').sum()
                if (df1.shape[1] == 0):
                    continue;
                if (df0.shape[0] == 1):
                    continue;
                # plot
                plt.title(name + ": Prox and Thermostat temp")
                plt.xlabel('Time', fontsize=14)
                # plt.ylabel('density', fontsize=14)
                # plt.figure(figsize=[20, 5])
                df0 = normalize(df0)
                df1 = normalize_bias(df1)
                plt.plot(df0, color='r', alpha=0.7)
                plt.plot(df1, color='g', alpha=0.3)
                # plt.legend(fontsize=14)
                # plt.tick_params(labelsize=14)
                plt.grid(True)
                plt.savefig('./static/q4_3_img/' + name)
                plt.close()
    return render(request, 'q4_3.html')


def bye(request):
    return render(request, 'ang.html')


def plot_hazium():
    h_f1 = pd.read_csv('./assign/Data/csv/f1z8a-MC2.csv')
    a = pd.DataFrame(h_f1['Date/Time '].values)
    b = pd.DataFrame(h_f1[' F_1_Z_8A: Hazium Concentration'].values)
    h_f1 = pd.concat([a, b], axis=1)
    h_f1.columns = ['Datetime', 'Hazium Concentration']
    h_f1.set_index('Datetime', inplace=True)

    H_f2_2 = pd.read_csv('./assign/Data/csv/f2z2-MC2.csv')
    a = pd.DataFrame(H_f2_2['Date/Time '].values)
    b = pd.DataFrame(H_f2_2[' F_2_Z_2: Hazium Concentration'].values)
    H_f2_2 = pd.concat([a, b], axis=1)
    H_f2_2.columns = ['Datetime', 'Hazium Concentration']
    H_f2_2.set_index('Datetime', inplace=True)

    H_f2_4 = pd.read_csv('./assign/Data/csv/f2z4-MC2.csv')
    a = pd.DataFrame(H_f2_4['Date/Time '].values)
    b = pd.DataFrame(H_f2_4[' F_2_Z_4: Hazium Concentration'].values)
    H_f2_4 = pd.concat([a, b], axis=1)
    H_f2_4.columns = ['Datetime', 'Hazium Concentration']
    H_f2_4.set_index('Datetime', inplace=True)

    H_f3 = pd.read_csv('./assign/Data/csv/f3z1-MC2.csv')
    a = pd.DataFrame(H_f3['Date/Time '].values)
    b = pd.DataFrame(H_f3[' F_3_Z_1: Hazium Concentration'].values)
    H_f3 = pd.concat([a, b], axis=1)
    H_f3.columns = ['Datetime', 'Hazium Concentration']
    H_f3.set_index('Datetime', inplace=True)

    temp = pd.concat([h_f1, H_f2_2, H_f2_4, H_f3], axis=1)
    temp.index = pd.to_datetime(temp.index)
    temp.columns = ['H_f1', 'H_f2_2', 'H_f2_4', 'H_f3']


def corr(df):
    corr = df.corr()
    corr.round(2)

    corr.index = df.columns

    plt.figure(figsize=(14, 14))
    sns.heatmap(corr, annot=True, cmap='RdBu_r', center=0)
    plt.show()
    plt.close()

    # 예측 대상 센서와 입력 센서간의 correlation만 출력
    corr_target_tags = corr.iloc[:-1, -1:]  # output tag가 2개인 경우
    plt.figure(figsize=(1, 8))
    sns.heatmap(corr_target_tags, annot=True, cmap='RdBu_r', center=0)
    plt.show()
    plt.close()


def plot_df(data, figdir):
    for sname in data.columns:
        plt.figure(figsize=[20, 5])
        plt.title(sname)
        plt.xlabel('Time', fontsize=14)
        plt.ylabel('Concentration', fontsize=14)
        plt.plot(data[sname], label=sname, color='red', alpha=1)
        plt.legend(fontsize=14)
        plt.tick_params(labelsize=14)
        plt.grid(True)
        plt.savefig('./static/' + figdir + sname, dpi=100)


def normalize_plot(df, figname):
    ndf = df.copy()
    for col in df.columns:
        a = ndf[col].max()
        ndf[col] = ndf[col].apply(lambda x: x / a)
    plot_overlap(ndf, figname)

def normalize(df):
    ndf = df.copy()
    for col in df.columns:
        a = ndf[col].max()
        ndf[col] = ndf[col].apply(lambda x: x / a)
    return ndf

def plot_overlap(data, figname):
    plt.title('Overlapped')
    plt.xlabel('Time', fontsize=14)
    plt.ylabel('density', fontsize=14)
    plt.figure(figsize=[20, 5])
    for col in data.columns:
        plt.plot(data[col], color='r', alpha=0.05)
        # plt.legend(fontsize=14)
        # plt.tick_params(labelsize=14)
    plt.grid(True)
    plt.savefig('./static/' + figname)


def plot_prox(data, figname):
    for col in data.columns:
        plt.figure(figsize=[15, 5])
        # plt.title(col)
        plt.xlabel('Time', fontsize=14)
        plt.ylabel(figname[8:] + '(# of people)', fontsize=14)
        plt.plot(data[col], alpha=0.5)  # label=col)#, color='red', alpha=1)
        # plt.legend(fontsize=14)
        # plt.tick_params(labelsize=14)
        plt.grid(True)
        plt.savefig('./static/' + figname)


def normalize_prox(df, figname):
    for col in df.columns:
        a = df[col].max()
        df[col] = df[col].apply(lambda x: x / a)
    plot_overlap(df, figname)


def plot_overlap_prox(data, figname):
    plt.title('Overlapped')
    plt.xlabel('Time', fontsize=14)
    plt.ylabel('density', fontsize=14)
    plt.figure(figsize=[20, 5])
    for col in data.columns:
        plt.plot(data[col], color='r', alpha=0.1)
        # plt.legend(fontsize=14)
        # plt.tick_params(labelsize=14)
    plt.grid(True)
    plt.savefig('./static/' + figname)


def make_prox_data():
    prox = pd.read_csv('./assign/Data/csv/proxOut-MC2.csv')
    prox.set_index(pd.to_datetime(prox.timestamp), inplace=True)
    del prox['timestamp']

    prox_total = prox.resample('5T').sum()
    prox_f1 = prox.loc[prox[' floor'] == 1]
    prox_f2 = prox.loc[prox[' floor'] == 2]
    prox_f3 = prox.loc[prox[' floor'] == 3]
    prox_f1_res = prox_f1.resample('5T').sum()
    prox_f2_res = prox_f2.resample('5T').sum()
    prox_f3_res = prox_f3.resample('5T').sum()

    # length matching
    n = pd.to_datetime('2016-05-31 00:00:00')
    prox_total.loc[n] = 0
    prox_total.sort_index(inplace=True)

    n = pd.to_datetime('2016-05-31 00:00:00')
    prox_f1_res.loc[n] = 0
    prox_f1_res.sort_index(inplace=True)

    n = pd.date_range('2016-05-31 00:00:00', '2016-05-31 06:55:00', freq='5T')
    for d in n:
        prox_f2_res.loc[d] = 0
    prox_f2_res.sort_index(inplace=True)

    n1 = pd.date_range('2016-05-31 00:00:00', '2016-05-31 06:55:00', freq='5T')
    n2 = pd.date_range('2016-06-13 18:05:00', '2016-06-13 23:55:00', freq='5T')
    for d in n1:
        prox_f3_res.loc[d] = 0
    for d in n2:
        prox_f3_res.loc[d] = 0
    prox_f3_res.sort_index(inplace=True)
    return prox_f1_res, prox_f2_res, prox_f3_res


def normalize_bias(df):
    for col in df.columns:
        a = df[col].max()
        df[col] = df[col].apply(lambda x: x/a)
        df[col] = df[col].apply(lambda x: x + (x-0.8)*2)
        b = df[col].max()
        df[col] = df[col].apply(lambda x: abs(x)/abs(b))
    return df

