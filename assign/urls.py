from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),

    url(r'^question3', views.question3, name='question3'),
    url(r'^q3_1', views.q3_1, name='q3_1'),
    url(r'^q3_2', views.q3_2, name='q3_2'),
    url(r'^q3_3', views.q3_3, name='q3_3'),
    url(r'^q3_4', views.q3_4, name='q3_4'),
    url(r'^q3_5', views.q3_5, name='hazium'),

    url(r'^question4', views.question4, name='question4'),
    url(r'^q4_1', views.q4_1, name='q4_1'),
    url(r'^q4_2', views.q4_2, name='q4_2'),
    url(r'^q4_3', views.q4_3, name='q4_3'),

]